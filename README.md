**Graduate Thesis: Violence Detection In Videos
Based On The Approach Of
Deep Multiple Instance
Learning Model**

Authors: 
        Le Quoc Thinh - UIT-VNUHCM, Vo Van Tuan - UIT-VNUHCM 

Proposed Top-K Multiple Instance Learning Ranking method for Violence Detection in Video Surveillance


**Contribution**


- Proposed Top MIL Ranking method
- Development of UIT-ViolenceCCTV dataset on violence in Vietnam
- Experimenting, evaluating and comparing Deep MIL Ranking, Complementary Inner Bag Loss and proposed method Top MIL Ranking
- Top MIL Ranking has become SOTA on the UIT-ViolenceCCTV dataset for all three metrics: AUC, Recall and False Alarm Rate
- Building a Web-Demo

**Dataset**

The UIT-ViolenceCCTV dataset (camera surveillance) includes 106 videos (30fps) with audio suppressed, with a total duration of about 155 minutes, equivalent to nearly 2,800 frames. Tranning set contains 84 videos and test sét contains 22 videos. The number of violent and normal videos in both sets was equal.

- Link Dataset: https://drive.google.com/drive/folders/1_9D9t9QuEpmvbh6iFAFjD4libbfZBVEM?usp=sharing

**Feature Extraction**

- C3D Extractor: **Learning Spatiotemporal Features with 3D Convolutional Networks** {https://arxiv.org/pdf/1412.0767.pdf}(Du Tran et al.)
- Extract C3D feature of video using Google Colab [https://gitlab.com/tuanvovanUIT/uit-violencedetectionsurveillance/-/blob/master/get_feauture_extractor.ipynb](url)
- C3D Pretrained Weight: https://drive.google.com/file/d/1F5L0cPOKRi5p7kTnDNQabf6ryt_rZUjK/view?usp=sharing

**Requirements**

Keras version 1.1.0

Theano 1.0.2

Python 3

Ubuntu 16.04

**Tranning**

- Tranning model with Top-K Multiple Instance Learning method using Google Colab: [https://gitlab.com/tuanvovanUIT/uit-violencedetectionsurveillance/-/blob/master/Train_Keras.ipynb](url)

- Model Top-K MIl Ranking Trained Weight: https://drive.google.com/drive/folders/1EXj6gbYyONiuRCO1rSo4q_iM4cv1-Soh?usp=sharing

**Evaluation**

- Evaluation with AUC-ROC curve, Recall and False Alarm Rate for Deep MIL Ranking, Complementary Inner Bag Loss and proposed method Top MIL Ranking using Google Colab:[https://gitlab.com/tuanvovanUIT/uit-violencedetectionsurveillance/-/blob/master/Visualize_Keras.ipynb](url) 

- Our proposed method - Top MIL Ranking - achieved an AUC of 80.11%, which is 4.23% higher than the baseline method and a Recall 6.88%, which is twice as high as the baseline method (3.16%). 
- Comparing AUC: https://drive.google.com/file/d/1AzDnas5GPzGtsKHRQTImBLHma1F93cqX/view?usp=sharing

**Visualize Results**
- Using Google Colab to Visualize result: [https://gitlab.com/tuanvovanUIT/uit-violencedetectionsurveillance/-/blob/master/Visualize_Keras.ipynb](url)

**Web Demo**
- Using Google Colab and ngrok for Development a Web Demo: [https://gitlab.com/tuanvovanUIT/uit-violencedetectionsurveillance/-/blob/master/Web.ipynb](url)
