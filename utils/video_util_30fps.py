from utils.array_util import *
import parameters as params
import cv2


def get_video_clips(video_path):
    frames = get_video_frames(video_path)
    clips = sliding_window(frames, params.frame_count, params.frame_count)
    return clips, len(frames)


def get_video_frames(video_path):
    cap = cv2.VideoCapture(video_path)
    # cap.set(cv2.CAP_PROP_FPS,15)
    # print(cap.get(cv2.CAP_PROP_FPS))
    # print("o day")
    # frames = []
    # while (cap.isOpened()):
    #     ret, frame = cap.read()
    #     if ret == True:
    #         frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    #         if (frame.shape[0]>240):
    #             #(frame.shape[0])
    #             #print(frame.shape[1])
    #             #print(int(240*frame.shape[1]/frame.shape[0]))
    #             frame = cv2.resize(frame, (240,int(240*frame.shape[1]/frame.shape[0])))
    #         frames.append(frame)
    #     else:
    #         break
    # return frames
    #def get_video_frames(video_path):
    #cap = cv2.VideoCapture(video_path)
    frames = []
    ret, frame = cap.read()
    if ret == True:
        if frame.shape[0] > 240:
            size1 = 240
            size2 = int(frame.shape[1]*240/frame.shape[0])
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frame = cv2.resize(frame, (size1,size2))
            frames.append(frame)
            while (cap.isOpened()):
                #cap.set(cv2.CAP_PROP_FPS,15)
                # cap.set(cv2.CAP_PROP_FPS,15)
                # print(cap.get(cv2.CAP_PROP_FPS))     
                ret, frame = cap.read()
                if ret == True:   
                    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                    frame = cv2.resize(frame, (size1,size2))
                    frames.append(frame)
                else:
                    break
        else:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frames.append(frame)
            while (cap.isOpened()):
                # cap.set(cv2.CAP_PROP_FPS,15)
                # print(cap.get(cv2.CAP_PROP_FPS))
                #cap.set(cv2.CAP_PROP_FPS,15)     
                ret, frame = cap.read()
                if ret == True:   
                    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                    frames.append(frame)
                else:
                    break
    return frames