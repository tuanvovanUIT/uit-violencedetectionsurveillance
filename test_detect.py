import os
from c3d import *
from classifier import *
from utils.visualization_util import *
from utils.video_util_30fps import *
from utils.array_util import *
from time import time
import numpy
from datetime import datetime
#import path
from os.path import basename
import glob
from os import listdir
import numpy as np
import cv2
from matplotlib import pyplot as plt
import os
from matplotlib.patches import Rectangle
import matplotlib.patches as patches

def figure2opencv(figure):
    figure.canvas.draw()
    img = np.fromstring(figure.canvas.tostring_rgb(), dtype=np.uint8, sep='')
    img = img.reshape(figure.canvas.get_width_height()[::-1] + (3,))
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    return img


# Thêm một param "feature" để xem có annotate nếu có
def GUI(video_parth,y_pred,save="Just save",s_path="output_video", annotate = None, threshold = 0.5):
    DISPLAY_IMAGE_SIZE = 500
    BORDER_SIZE = 20
    FIGHT_BORDER_COLOR = (0, 0, 255)
    NO_FIGHT_BORDER_COLOR = (0, 255, 0)

    # violenceDetector = ViolenceDetector()
    videoReader = cv2.VideoCapture(video_parth)
    #test giam fps
    # frame_count = int(videoReader.get(cv2.CAP_PROP_FRAME_COUNT))
    # fps = videoReader.get(cv2.CAP_PROP_FPS)
    # fps_target = 3
    # scale = fps/fps_target
    # max_range = int(frame_count*fps_target/fps+0.99)
    # list_item = [int(x*scale) for x in range(max_range)]
    #testgiam fps

    isCurrentFrameValid, currentImage = videoReader.read()

    #print('--------------',s_path)
    if save == True or save == "Just save":
        #fps = videoReader.get(cv2.CV_CAP_PROP_FPS)
        fps = videoReader.get(cv2.CAP_PROP_FPS)
        # fourcc = cv2.VideoWriter_fourcc(*'MP4V')
        # out = cv2.VideoWriter(s_path, fourcc,fps,(600, 300))
        fourcc = cv2.VideoWriter_fourcc(*'vp80')
        out = cv2.VideoWriter(s_path, fourcc,fps,(600, 300))

    #farme_window=[0]*clip_length
    fig = plt.figure()
    farme_cout = 0
    # length = len(y_pred)
    length = int(videoReader.get(cv2.CAP_PROP_FRAME_COUNT))
    # T = 0
    while isCurrentFrameValid:
        # if (T in list_item):
        targetSize = DISPLAY_IMAGE_SIZE - 2 * BORDER_SIZE
        currentImage = cv2.resize(currentImage, (targetSize, targetSize))

        if y_pred[farme_cout] > threshold:
            resultImage = cv2.copyMakeBorder(currentImage,
                                        BORDER_SIZE,
                                        BORDER_SIZE,
                                        BORDER_SIZE,
                                        BORDER_SIZE,
                                        cv2.BORDER_CONSTANT,
                                        value=FIGHT_BORDER_COLOR)
        else:
            resultImage = cv2.copyMakeBorder(currentImage,
                                        BORDER_SIZE,
                                        BORDER_SIZE,
                                        BORDER_SIZE,
                                        BORDER_SIZE,
                                        cv2.BORDER_CONSTANT,
                                        value=NO_FIGHT_BORDER_COLOR)

        resultImage = cv2.resize(resultImage, (300, 300))

        #fig = plt.figure()
        # print(pec_store)
        #print(farme_cout)
        #print(y_pred[farme_cout-1])
        if farme_cout > 0:
            plt.plot([farme_cout-1,farme_cout], y_pred[farme_cout-1:farme_cout+1], color='green', marker='o', linestyle='-.', linewidth=2, markersize=2)
        # plt.plot(it_uesed, pec_store2, c="r")
        # plt.xlim()
        plt.ylim(0, 1.0)
        plt.xlim(0, length)
        plot_img = figure2opencv(fig)
        plot_img = cv2.resize(plot_img, (300, 300))  # (0, 0), None, .25, .25)

        resultImage = np.concatenate((resultImage, plot_img), axis=1)

        farme_cout = farme_cout + 1
        
        #if save == True or save=="Just save":
            # Write the frame into the file 'output.avi'
            #print("saving")
        out.write(resultImage)

        # if save!="Just save":
        #     cv2.imshow("Violence Detection", resultImage)
        # else:
        if farme_cout % 100 == 0:
            print(str(farme_cout)+"/"+str(length))

        #userResponse = cv2.waitKey(1)
        #if userResponse == ord('q'):
            #videoReader.release()
            #cv2.destroyAllWindows()
            #break

        #else:

        isCurrentFrameValid, currentImage = videoReader.read()
            # T+=1
        # else:
        #     isCurrentFrameValid, currentImage = videoReader.read()
        #     T+=1


        # T+=1
        # while T not in list_item:
        #     isCurrentFrameValid, currentImage = videoReader.read()
        #     T+=1

    
    

    #if save == True or save == "Just save":
    videoReader.release()
    out.release()

def ExportHighScore(video_parth, y_pred, s_path="output_video", threshold = 0.5):
    # violenceDetector = ViolenceDetector()
    videoReader = cv2.VideoCapture(video_parth)
    # giam fps
    # frame_count = int(videoReader.get(cv2.CAP_PROP_FRAME_COUNT))
    # fps = videoReader.get(cv2.CAP_PROP_FPS)
    # fps_target = 3
    # scale = fps/fps_target
    # max_range = int(frame_count*fps_target/fps+0.99)
    # list_item = [int(x*scale) for x in range(max_range)]
    # giam fps
    isCurrentFrameValid, currentImage = videoReader.read()

    fps = videoReader.get(cv2.CAP_PROP_FPS)        
    # fourcc = cv2.VideoWriter_fourcc(*'XVID')
    # fps = videoReader.get(cv2.CAP_PROP_FPS)
        # fourcc = cv2.VideoWriter_fourcc(*'MP4V')
        # out = cv2.VideoWriter(s_path, fourcc,fps,(600, 300))
    fourcc = cv2.VideoWriter_fourcc(*'vp80')
    # out = cv2.VideoWriter(s_path, fourcc,fps,(600, 300))
    
    width = int(videoReader.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(videoReader.get(cv2.CAP_PROP_FRAME_HEIGHT))
    
    if height > 300:
        width = int(width*300/height)
        height = 300
    # print(s_path)
    out = cv2.VideoWriter(s_path, fourcc, fps, (width, height))
        
        
    if np.sum(y_pred > threshold) == 0:
        temp = cv2.imread(r'/content/gdrive/My Drive/DoAnChuyenNganh/HumanBehaviorBKU/temp/Temp.jpg')
        temp_ = cv2.resize(temp, (width,height))
        # plt.imshow(temp)

        for i in range(20):
            out.write(temp_)
        out.release()
        return
    else:    
        farme_cout = 0
        length = int(videoReader.get(cv2.CAP_PROP_FRAME_COUNT))
        # length = len(y_pred)
        print("++++++++===",len(y_pred))
        # T = 0
        while isCurrentFrameValid:
            # if (T in list_item):
            if y_pred[farme_cout] > threshold:
                currentImage = cv2.resize(currentImage, (width, height))
                out.write(currentImage)
            if farme_cout < length:
                farme_cout = farme_cout + 1
            else:
                break
            if farme_cout % 50 == 0:
                print(str(farme_cout)+"/"+str(length))
            isCurrentFrameValid, currentImage = videoReader.read()
                # T+=1
            # else:
            #     isCurrentFrameValid, currentImage = videoReader.read()
            #     T+=1
                 
        out.release()
    videoReader.release()
def run_demo(pathin,pathout,pathscore):

    video_name = os.path.basename(pathin).split('.')[0]

    # read video
    start = time()
    video_clips, num_frames = get_video_clips(pathin)

    #print("Number of clips in the video : ", len(video_clips))

    # build models
    feature_extractor = c3d_feature_extractor()
    classifier_model = build_classifier_model()

    #print("Models initialized")

    # extract features
    
    rgb_features = []
    for i, clip in enumerate(video_clips):
        clip = np.array(clip)
        if len(clip) < params.frame_count:
            continue

        clip = preprocess_input(clip)
        rgb_feature = feature_extractor.predict(clip)[0]
        rgb_features.append(rgb_feature)

        print("Processed clip : ", i)

    rgb_features = np.array(rgb_features)

    # bag features
    rgb_feature_bag = interpolate(rgb_features, params.features_per_bag)

    # classify using the trained classifier model
    predictions = classifier_model.predict(rgb_feature_bag)

    predictions = np.array(predictions).squeeze()

    predictions = extrapolate(predictions, num_frames)
    #print(time()-start)

    # save_path = os.path.join(cfg.output_folder, video_name + '.gif')
    # # visualize predictions
    # #print('Executed Successfully - '+video_name + '.gif saved')
    # #visualize_predictions(pathin, predictions, save_path)
    # frames = get_video_frames(pathin)
    # #frames_anomaly = []
    # for i in range(len(predictions)):
    #     print("frame %s, score: "%i,predictions[i])
    #     if(predictions[i]>0.6):
    #         #cv2.imwrite(cfg.output_folder+'/frameanomaly/anomalyfarme%s.jpg' %i,frames[i])
    #         frames[i] = cv2.rectangle(frames[i], (2,2), (frames[i].shape[1]-2, frames[i].shape[0]-2), (0,0,255), 8)
    #         #frames_anomaly.append(frames[i])
    # height, width,_ = frames[0].shape
    # size = (width,height)
    # #clips = sliding_window(frames_anomaly, params.frame_count, params.frame_count)
    # out = cv2.VideoWriter(pathout,cv2.VideoWriter_fourcc(*'H264'), 30, size)
    # for i in range(len(frames)):
    #     out.write(frames[i])
    # out.release()
    #name = os.path.split(pathin)[1]
    #name = name.split('.')[0]
    GUI(pathin, predictions,save="Just save",s_path=pathout, annotate = None)
    ExportHighScore(pathin, predictions,s_path=pathscore)
if __name__ == '__main__':
        run_demo(r'F:\My Drive\DoAnChuyenNganh\AnomalyDetectionCVPR2018-Pytorch\update_data\Violence_Test\Violence_043_1.mp4',r'F:\My Drive\DoAnChuyenNganh\AbnormalEventDetection\output\output.webm',r'F:\My Drive\DoAnChuyenNganh\AbnormalEventDetection\output\score.webm')